from netCDF4 import Dataset
import os
#indextmax=87
#indexxmax=98

indextmax=8760
indexxmax=9892
indextoffsetJanuary=3666
step=indextmax-indextoffsetJanuary

PATH_INPUT_TO_DATA_SAFRAN="/home/olivierb/aedes/SAFRAN/DATA/"

#PATH_INPUT_TO_DATA_SAFRAN="/media/olivierb/0990d57f-9a9a-41d7-b7b4-5cc7a3135e5e/SAFRAN/"
PATH_OUTPUT_TO_DATA_SAFRAN="/home/olivierb/aedes/SAFRAN/TEMP/"
if not os.path.exists(PATH_OUTPUT_TO_DATA_SAFRAN):
    os.makedirs(PATH_OUTPUT_TO_DATA_SAFRAN)

years=[2009]
#years=[2010,2011,2012]

#chaque serie_numAxx(numA+1).nc contient  1 an de donnees (8760 heures) commencant le 1 aout a 6h:
# les suffix de fichiers vont de 1 a 8760, ici le suffix est represente par step
#    annee numA:
#    -----------
#     nbr heure du 1 aout a 6h aux 31 decembre a minuit=152 j + 6 h=152*24 + (24-6)=3666
#     correspondnat aux suffix {8760-3666+1,...,8760}
#     annee numA+1:
#    -------------
#     nbr heure du 1 janvier a 0h aux 1 aout a 6h= 8760-3666
#     correspondnat aux suffix {0,...,8760-3666}

for numA in years:
    step=indextmax-indextoffsetJanuary+1
    dirOutput=PATH_OUTPUT_TO_DATA_SAFRAN+str(numA)+"/"
    if not os.path.exists(dirOutput):
        os.makedirs(dirOutput)
    datasafran = Dataset(PATH_INPUT_TO_DATA_SAFRAN+"SAFRAN_"+str(numA)+"080107_"+str(numA+1)+"080106.nc")
    Tair=datasafran.variables[u'Tair']
    indext=0
    while indext<indextmax:
        if (step>indextmax):
            step=1
            dirOutput=PATH_OUTPUT_TO_DATA_SAFRAN+str(numA+1)+"/"
            if not os.path.exists(dirOutput):
                os.makedirs(dirOutput)
        if (step > 7024):
            f = open(dirOutput+"TAIR_"+str(step), "wb")
            indexx=0
            while indexx<indexxmax:
                f.write(Tair[indext,indexx])
                indexx=indexx+1
            f.close()
        indext=indext+1
        step=step+1


