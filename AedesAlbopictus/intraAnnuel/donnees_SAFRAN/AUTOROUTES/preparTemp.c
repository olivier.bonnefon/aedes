#include  <iostream>
#include <string>
#include <stdio.h>
#include <fstream>
#include <stdlib.h>
#include <string.h>
#include <math.h>

using namespace std;
#include "../defRasterSafran.h"
//#define DEBUGING
#ifdef DEBUGING
#include "visuTiff.h"
#endif
#define NB_PT_SAFRAN 9892
#define NB_TIME_SAFRAN 8760
#define FIRST_YEAR 2008
#define LAST_YEAR 2012

#define PATH_TO_DATA_RASTER "/home/olivierb/aedes/SAFRAN/DATA_RASTER/"
#define PATH_TO_DATA_FF "/home/olivierb/aedes/SAFRAN/DATA_FF/"

long int meshIndex[15]={6295,1612,2457,1116,2222,1968,1528,938,2246,1091,1058,1325,285,164,1069};

int readXY(double **pMeshX,double **pMeshY){
  long int dim;
  double *pY,*pX;

  ifstream xx("X",ios_base::binary);
  ifstream yy("Y",ios_base::binary);
  xx.read((char *) &dim, sizeof(long int));
  yy.read((char *) &dim, sizeof(long int));
  cout<<"reads dim="<<dim<<endl;
  pX=(double *)malloc(dim*sizeof(double));
  pY=(double *)malloc(dim*sizeof(double));
  *pMeshX=pX;*pMeshY=pY;
  for(long int i=0; i<dim; i++)
  {
    xx.read((char *) (pX++), sizeof(double));
    yy.read((char *) (pY++), sizeof(double));
  }
  return dim;
}
//project raster data to mesh
int readTemperatures(double *pData,double *meshX,double *meshY,int dim,int numA){
  char fileName[512];
  double *pDataAux;
  int h,k;
  int ncols=NX+1;
  int nrows=NY+1;
  double xllcorner=XMIN;
  double yllcorner=YMIN;
  double cellsize=DX;
  double val;
  double minT=1000;
  double maxT=0;
  pDataAux=pData;
  double *pT=(double*)calloc(nrows*ncols,sizeof(double));
  //double *pTaux=(double*)calloc(nrows*ncols,sizeof(double));
  int nbTimeRead=0;
  for (h=1;h<=NB_TIME_SAFRAN;h++){
    //lecture du raster
    sprintf(fileName,"%s/%i/TAIR_%i",PATH_TO_DATA_RASTER,numA,h);
    FILE *infile=fopen(fileName,"rb");
    if (!infile)
      continue;
    nbTimeRead++;
    int nr=fread(pT, sizeof(double), ncols*nrows, infile);
    if (nr!=ncols*nrows){
      printf("Error fread return value= % instead of %i\n",nr,ncols*nrows);
      return -1;
    }
#ifdef DEBUGING
    memcpy(pTaux,pT,sizeof(double)* ncols*nrows);
    for (k=0;k<ncols*nrows;k++){
      if (pTaux[k]<minT)
        minT=pTaux[k];
      if (pTaux[k]>maxT)
        maxT=pTaux[k];
    }
    printf ("minT=%lf maxT=%lf\n",minT,maxT);
    for (k=0;k<ncols*nrows;k++)
      pTaux[k]=pTaux[k]/maxT;
    visuTiff_Init(1,ncols,nrows);
    visuTiff_Register(pTaux,0);
    visuTiff_End();
    break;
#endif
      
      
    fclose(infile);
    //creation du champ de temperature sur le maillage
    for(long int i=0; i<dim; i++)
    {
      double bufT;
      double Y=meshY[i];
      double X=meshX[i];
      int icol=(X-xllcorner)/((1+1e-6)*cellsize);
      int irow=(Y-yllcorner)/((1+1e-6)*cellsize);
      int index=icol+(irow)*ncols;
      if (index >= nrows*ncols || index <0){
        cout<<"bug "<<index<<" soulb be <"<<nrows*ncols;
        return -1;
      }
      bufT=pT[index];
      *(pDataAux++)=bufT-273.15;
    }
  }
  
  free(pT);
  return nbTimeRead;
}
/*
 *
 *dim       : nombre de neuds du maillage
 *T0        : temperature seuil
 *Nd        : un nombre d'heures 
 *pDataDegJ : output  pDataDegJ[t,inode]=min(Nd,nombre d'heures antérieur a t ayant une temperature > T0)
 */
void computeDegreJ(double T0,double Nd,double *pDataT,int dim, double * pDataDegJ){
  for (long int i=0;i<dim;i++)
    if ((*(pDataT++))>T0)
      *(pDataDegJ++)=1;
    else
      *(pDataDegJ++)=0;
  
  for (int h=1;h<365*24;h++){
    memcpy(pDataDegJ,pDataDegJ-dim,dim*sizeof(double));
    for (long int i=0;i<dim;i++){
      if ((*(pDataT++))>T0 && (*pDataDegJ) <Nd)
        (*pDataDegJ)+=1;
      pDataDegJ++;
    }
  }
  
}

/*
 *
 *dim       : nombre de neuds du maillage
 *T0        : temperature seuil
 *pT0J : output   GDD(s) pour s=0 a nbDay-59
 */

void computeGDD(double T0,double *pDataT,int dim, double * pT0J,int nbDay){
  //on se place le 28 fevrier
  pDataT+=59*24*dim;
  double *psaveT0J=pT0J;

  //pour chaque jours s a partir de 1 mars
  for (int s=59;s<nbDay;s++){
    //initialise a 0
    for (long int i=0;i<dim;i++)
      pT0J[i]=0;
    //contribution des 24 heures du jours s
    for (int h=0;h<24;h++)
      for (long int i=0;i<dim;i++)
        pT0J[i]+=(*(pDataT++));
    //calcul du GDD
    for (long int i=0;i<dim;i++)
      if (pT0J[i] > T0*24)
        pT0J[i]=(pT0J[i]/24.0)-T0;
      else
        pT0J[i]=0;
    //ajout de la contribution des jours precedents
    if (s>59)
       for (long int i=0;i<dim;i++)
         pT0J[i]+=pT0J[i-dim];
//    memcpy(pT0J+dim,pT0J,dim*sizeof(double));
    pT0J=pT0J+dim;
  }

}

void seuilGDD(double GDD_Threshold,int dim,double *pIn, double *pOut,int nbDay){
  for (int s=59;s<nbDay;s++)
    for (long int i=0;i<dim;i++){
      if (*pIn++>GDD_Threshold)
        *pOut++=1;
      else
        *pOut++=0;
    }
}

/*
 *
 *dim       : nombre de neuds du maillage
 *T0        : temperature seuil
 *GDD_Threshold        : seuil du GDD : GDD(j)=Sigma_ (j > s > 59 jours) max{T(s,x,y)-10,0} 
 *pT0J : output  pDataDegJ[s,inode]=0 si GDD(s) < GDD_Threshold sinon 1 pour s=0 a 365-59
 */
//obsolete
void computeT0usingGDD(double T0,double GDD_Threshold,double *pDataT,int dim, double * pT0J){
  //on se place le 28 fevrier
  pDataT+=59*24*dim;
  double *psaveT0J=pT0J;

  //pour chaque jours s a partir de 1 mars
  for (int s=59;s<365;s++){
    //initialise a 0
    for (long int i=0;i<dim;i++)
      pT0J[i]=0;
    //contribution des 24 heures du jours s
    for (int h=0;h<24;h++)
      for (long int i=0;i<dim;i++)
        pT0J[i]+=(*(pDataT++));
    //calcul du GDD
    for (long int i=0;i<dim;i++)
      if (pT0J[i] > T0*24)
        pT0J[i]=(pT0J[i]/24.0)-T0;
      else
        pT0J[i]=0;
    //ajout de la contribution des jours precedents
    if (s>59)
       for (long int i=0;i<dim;i++)
         pT0J[i]+=pT0J[i-dim];
    memcpy(pT0J+dim,pT0J,dim*sizeof(double));
    pT0J=pT0J+dim;
  }

  //1(GDD>GDD_Threshold)
  pT0J=psaveT0J;
  for (int s=59;s<365;s++){
    for (long int i=0;i<dim;i++){
      if (pT0J[i]>GDD_Threshold)
        pT0J[i]=1;
      else
        pT0J[i]=0;
      
    }
    pT0J+=dim;
  }
  
}
/*
 *
 *previous version piecewise linear
 *
 *
 *
 *  0 if <10
 *  1 if >25
 *  else linear and continuous
 *
 */
void computeGTWithPiceWiseLinear(double *pDataT,int dim,double *pGT){
  double *pCurGT=pGT;
  double *pAux;
  for(int j=0;j<365;j++){
    pAux=pCurGT;
    for (long int i=0;i<dim;i++)
      *(pAux++)=0;
    for (int h=0;h<24;h++){
      pAux=pCurGT;
      for (long int i=0;i<dim;i++){
        double temp=*(pDataT++);
        if (temp<10){
          *(pAux++)+=0;
        }else if (temp<25){
          *(pAux++)+=(temp-10)/15.0;
        }else{
          *(pAux++)+=1;
        }
      }
    }
    for (long int i=0;i<dim;i++)
      (*(pCurGT++))/=24.0;
  }
}



/*
10.804361785896965   0.080201779776833  12.438168144101718

  Columns 4 through 5

  40.439255430520049  -0.145790284196338 
 */

double psi_median=10.804361785896965;
double alpha_median=0.080201779776833;
double delta_median=12.438168144101718;
double Tmax_median=40.439255430520049;
double lambda_median= -0.145790284196338 ;
/*this is the */
double rhofct_median(double T){
  double res=psi_median*(exp(alpha_median*T)-exp(alpha_median*Tmax_median-(Tmax_median-T)/delta_median))+lambda_median;
  if (res<0)
    res=0;
  return res;
}

double psi=11.128007398517115;
double alpha=0.078865315431034;
double delta=12.651026892598505;
double Tmax=42.345187120388388;
double lambda=-0.145204446756075 ;
/*compute GT like sigma_24h rho(T)*/
double rhofct(double T){
  double res=psi*(exp(alpha*T)-exp(alpha*Tmax-(Tmax-T)/delta))+lambda;
  if (res<0)
    res=0;
  return res;
}


/*compute GT from the 24h meduim temperature, using the approximation rhofct_median*/
void computeGT_median(double *pDataT,int dim,double *pGT,int nbDay){
  double *pCurGT=pGT;
  double *pAux;
  for(int j=0;j<nbDay;j++){
    pAux=pCurGT;
    for (long int i=0;i<dim;i++)
      *(pAux++)=0;
    for (int h=0;h<24;h++){
      pAux=pCurGT;
      for (long int i=0;i<dim;i++){
        double temp=*(pDataT++);
        *(pAux++)+=temp;
      }
    }
    for (long int i=0;i<dim;i++){
      (*pCurGT)/=24.0;
      *pCurGT=rhofct_median(*pCurGT);
      pCurGT++;
    }
    
  }
}
void computeGT(double *pDataT,int dim,double *pGT,int nbDay){
  double *pCurGT=pGT;
  double *pAux;
  for(int j=0;j<nbDay;j++){
    pAux=pCurGT;
    for (long int i=0;i<dim;i++)
      *(pAux++)=0;
    for (int h=0;h<24;h++){
      pAux=pCurGT;
      for (long int i=0;i<dim;i++){
        double temp=*(pDataT++);
        *(pAux++)+=rhofct(temp);
      }
    }
    for (long int i=0;i<dim;i++){
      (*pCurGT)/=24.0;
      pCurGT++;
    }
    
  }
}
void computeAverageT(double Period,double *pDataT,int dim,double *pDataAverage){
  double *pBuf=(double *)calloc(dim,sizeof(double));
  double *pAux=pBuf;
  double *pCur=pDataT;
  double *pAux2=pDataAverage;
  int h;
  for (h=0;h<Period;h++){
    pAux=pBuf;
    for (long int i=0;i<dim;i++){
      *(pAux++)+=(*pCur++);
      *(pDataAverage++)=0;
    }
  }
  double *pCurMinusT=pDataT;
  for(;h<24*365;h++){
    memcpy(pDataAverage,pBuf,dim*sizeof(double));
    pDataAverage+=dim;
    pAux=pBuf;
    for (long int i=0;i<dim;i++){
      *(pAux++)+=(*pCur++)-(*pCurMinusT++);
    }
  }
  free(pBuf);
  for(h=0;h<24*365*dim;h++){
    *pAux2=*pAux2/Period;
    pAux++;
  }
}
void computeAverageJanuaryT(double *pDataT,int dim, double *pBuf){
  
  double *pAux;
  for (int h=0;h<30*24;h++){
    pAux=pBuf;
    for (long int i=0;i<dim;i++){
      *(pAux++)+=(*pDataT++);
    }
  }
  pAux=pBuf;
  for (long int i=0;i<dim;i++){
    *pAux=*pAux/(24*30*1.0);
    pAux++;
  }
  

  
}
#include <sys/dir.h>
#include <sys/stat.h>
#include <sys/types.h>

void saveField( double *pDataAux,int numA,char * fieldString,int nbTimeSteps){
  char dirName[256];
  char fileName[256];
  int h;
  int curYear=0;
  int curH=0;
  double bufT;
  
  mkdir(PATH_TO_DATA_FF,0755);
  sprintf(dirName,"/%s/%s/",PATH_TO_DATA_FF,fieldString);mkdir(dirName,0755);
  sprintf(fileName,"%s/%i",dirName,numA);mkdir(fileName,0755);
  for ( h=1;h<=nbTimeSteps;h++){
   
    for (int nth=0;nth<15;nth++){
      long int auxdim=meshIndex[nth];
      sprintf(fileName,"%s/%i/mesh%is%i",dirName,numA,nth,h);
      ofstream outfile (fileName,ios_base::binary);
      outfile.write ((char*) &auxdim, sizeof(long int));
      for(long int i=0; i<auxdim; i++){
        bufT=*(pDataAux++);
        outfile.write ((char*) &bufT, sizeof(double));
      }
//      curIndexInDim+=auxdim;
      outfile.close();
    }
  }
}
int main(){
  char fileName[512];
  char dirName[512];
  char fileNameBegin[512];
  char dirNameBegin[512];
  char fileNameEnd[512];
  char dirNameEnd[512];
  char dirNameGP[512];
  char fileNameGP[512];
  int ncols;
  int nrows;
  double xllcorner;
  double yllcorner;
  double cellsize;
  double val;
  double minT=1000;
  double *pDatas,*pT0J,*pDatasAverage,*pGT,*pT0JSeuil;
  double *pDataAux;
  long int dim;
  double *meshY,*meshX;
  double bufT;
  int offsetJanuary=3666;
  dim=readXY(&meshX,&meshY);
  pDatas=(double *)malloc(dim*NB_TIME_SAFRAN*sizeof(double));//temperature pDatas(num,h)=pDatas[num + h*dim];
  pT0J=(double *)malloc(dim*NB_TIME_SAFRAN*sizeof(double));//temperature pDatas(num,h)=pDatas[num + h*dim];
  pT0JSeuil=(double *)malloc(dim*NB_TIME_SAFRAN*sizeof(double));//temperature pDatas(num,h)=pDatas[num + h*dim];
  pGT=(double *)malloc(dim*NB_TIME_SAFRAN*sizeof(double));//temperature pDatas(num,h)=pDatas[num + h*dim];
  pDatasAverage=(double *)malloc(dim*NB_TIME_SAFRAN*sizeof(double));
  double *pBuf=(double *)calloc(dim,sizeof(double));
  for (int numA=FIRST_YEAR;numA<=LAST_YEAR;numA++){

    int nbTimeRead=readTemperatures(pDatas,meshX,meshY,dim,numA);
    saveField(pDatas,numA,"T_BY_HEURE",nbTimeRead);
    if (nbTimeRead!=NB_TIME_SAFRAN){
      printf("Warning, year=%i has %i times instead of %i.\n",numA,nbTimeRead,NB_TIME_SAFRAN);
    }
    int nbDays=nbTimeRead/24;
    computeGDD(10,pDatas,dim, pT0J,nbDays);
    saveField(pT0J,numA,"GDD_BY_DAY",nbDays-59);
    seuilGDD(294,dim,pT0J,pT0JSeuil,nbDays);
    saveField(pT0JSeuil,numA,"GDD_294",nbDays-59);
    seuilGDD(269,dim,pT0J,pT0JSeuil,nbDays);
    saveField(pT0JSeuil,numA,"GDD_269",nbDays-59);
    seuilGDD(246,dim,pT0J,pT0JSeuil,nbDays);
    saveField(pT0JSeuil,numA,"GDD_246",nbDays-59);
   
    
    computeGT_median(pDatas,dim,pGT,nbDays);
    saveField(pGT,numA,"GT_BY_DAY",nbDays);
    computeGT(pDatas,dim,pGT,nbDays);
    saveField(pGT,numA,"GT_BY_DAY_HOURLY",nbDays);
//    computeT0usingGDD(10,5,pDatas,dim, pT0J);
//    saveField(pT0J,numA,"GDDTHRESHOLD_BY_DAY",365-59);
    //continue;
    
//temperature pDatas(num,h)=pDatas[num + h*dim];
    //ie, pDatas[num + h*dim]; est la température à l'heure h du sommet num
    if (1){
      //int nbTimeRead=readTemperatures(pDatas,meshX,meshY,dim,numA);
      //saveField(pDatas,numA,"T_BY_HEURE",NB_TIME_SAFRAN);
    
      // a revoir
      /*computeDegreJ(17,240,pDatas,dim,pDatasDegJ);
        saveField(pDatasDegJ,numA,"DEGJ_BY_HEURE");*/

      //GDD threshold
      
      //in fact T0 is not used in the simulateur
      //computeT0usingGDD(10,5,pDatas,dim, pT0J);
      //saveField(pT0J,numA,"GDDTHRESHOLD_BY_DAY",365-59);
  
      //computeAverageT(7*24,pDatas,dim,pDatasAverage);
      //saveField(pDatasAverage,numA,"AVERAGE_BY_HEURE",NB_TIME_SAFRAN);

      sprintf(dirName,"%s/JANUARY_AVERAGE/",PATH_TO_DATA_FF);
      mkdir(dirName,0755);
      sprintf(dirName,"%s/JANUARY_AVERAGE/%i/",PATH_TO_DATA_FF,numA);
      mkdir(dirName,0755);
    
      computeAverageJanuaryT(pDatas,dim,pBuf);
      pDataAux=pBuf;
//    int curIndexInDim=0;
  
      for (int nth=0;nth<15;nth++){
      
        long int auxdim=meshIndex[nth];
        sprintf(fileName,"%s/mesh%i",dirName,nth);
        ofstream outfile (fileName,ios_base::binary);
        outfile.write ((char*) &auxdim, sizeof(long int));
        for(long int i=0; i<auxdim; i++){
          bufT=*(pDataAux++);
          outfile.write ((char*) &bufT, sizeof(double));
        }
        //    curIndexInDim+=auxdim;
        outfile.close();
      }
      /*
        sprintf(fileName,"DATA_%i/januaryAverage_%i",numA+1,numA+1);
        ofstream outfile (fileName,ios_base::binary);
        outfile.write ((char*) &dim, sizeof(long int));
        pDataAux=pBuf;
        for(long int i=0; i<dim; i++)
        {
        bufT=*(pDataAux++);
        outfile.write ((char*) &bufT, sizeof(double));
        }
        outfile.close();
      */
    }
  }
  //lecture des periode de photoperiode pour la lat 52 et 42
  if (0){
    ifstream filelat52 ("/home/olivierb/solvers/trunk/SandBox/FF/AedesAlbopictus/photoperiode/lat52.csv");
    ifstream filelat42 ("/home/olivierb/solvers/trunk/SandBox/FF/AedesAlbopictus/photoperiode/lat42.csv");
    double photomax[365];
    double photomin[365];
    if (filelat52 && filelat42){
      sprintf(dirName,"%s/PHOTO_PERIOD_BY_DAY/",PATH_TO_DATA_FF);
      mkdir(dirName,0755);
      sprintf(dirNameEnd,"%s/END_DIAPOSE_BY_DAY/",PATH_TO_DATA_FF);
      mkdir(dirNameEnd,0755);
      sprintf(dirNameBegin,"%s/BEGIN_DIAPOSE_BY_DAY/",PATH_TO_DATA_FF);
      mkdir(dirNameBegin,0755);
      sprintf(dirNameGP,"%s/GP_BY_DAY/",PATH_TO_DATA_FF);
      mkdir(dirNameGP,0755);

      double ymin=1e30;
      double ymax=-1e30;
      for (int i=0;i<dim;i++){
        if (meshY[i]>ymax)
          ymax=meshY[i];
        if (meshY[i]<ymin)
          ymin=meshY[i];
      }
        
      for (int j=0;j<365;j++){
        filelat52>>photomax[j];
        filelat42>>photomin[j];
      }
      //valeur max permettant la diapose de debut d'annee
      double P0=10;
      //valeur max permettant la diapose de fin d'année
      double P1=9.5;
      for (int j=0;j<365;j++){
        pDataAux=meshY;
        for (int nth=0;nth<15;nth++){
          long int auxdim=meshIndex[nth];
          sprintf(fileName,"%s/j%is%i",dirName,nth,j);
          sprintf(fileNameEnd,"%s/end%is%i",dirNameEnd,nth,j);
          sprintf(fileNameBegin,"%s/begin%is%i",dirNameBegin,nth,j);
          sprintf(fileNameGP,"%s/gp%is%i",dirNameGP,nth,j);
          ofstream outfile (fileName,ios_base::binary);
          ofstream outfileEnd (fileNameEnd,ios_base::binary);
          ofstream outfileBegin (fileNameBegin,ios_base::binary);
          ofstream outfileGp (fileNameGP,ios_base::binary);
          outfile.write ((char*) &auxdim, sizeof(long int));
          outfileEnd.write ((char*) &auxdim, sizeof(long int));
          outfileBegin.write ((char*) &auxdim, sizeof(long int));
          outfileGp.write ((char*) &auxdim, sizeof(long int));
          for (int i=0;i<auxdim;i++){
            double coef=(ymax-*pDataAux)/(ymax-ymin);
            double aux=coef*photomin[j]+(1-coef)*photomax[j];
            outfile.write ((char*) &aux, sizeof(double));
            double gp;
            if (aux<10)
              gp=0;
            else if (aux<14)
              gp=(aux-10)/4.0;
            else
              gp=1;
            outfileGp.write ((char*) &gp, sizeof(double));
            //entre en diapose
            double beginDiaspos;
            //sortie de diapose
            double endDiapos;
            beginDiaspos=0;
            if (j>170 && aux < P1)
              beginDiaspos=1;
            endDiapos=0;
            if (aux > P0)
              endDiapos=1;
            outfileBegin.write ((char*) &beginDiaspos, sizeof(double));
            outfileEnd.write ((char*) &endDiapos, sizeof(double));
            pDataAux++;
          }
          outfile.close();
          outfileEnd.close();
          outfileBegin.close();
          outfileGp.close();
        }
        
      }
    }else
      printf("cant find lat52 or lat42\n");
  }
  free(pBuf);
  free(pDatasAverage);
  free(pDatas);
  free(pT0J);
  free(pGT);
  return 0;
}
