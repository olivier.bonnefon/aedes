#include  <iostream>
#include <string>
#include <stdio.h>
#include <fstream>
#include <stdlib.h>
#include <string.h>
#include "visuTiff.h"
using namespace std;
#include "defRasterSafran.h"
#define DEBUGING 0
int readXY(double **pMeshX,double **pMeshY){
  long int dim;
  double *pY,*pX;

  ifstream xx("X",ios_base::binary);
  ifstream yy("Y",ios_base::binary);
  xx.read((char *) &dim, sizeof(long int));
  yy.read((char *) &dim, sizeof(long int));
  cout<<"reads dim="<<dim<<endl;
  pX=(double *)malloc(dim*sizeof(double));
  pY=(double *)malloc(dim*sizeof(double));
  *pMeshX=pX;*pMeshY=pY;
  for(long int i=0; i<dim; i++)
  {
    xx.read((char *) (pX++), sizeof(double));
    yy.read((char *) (pY++), sizeof(double));
  }
  return dim;
}
//project raster data to mesh
void readTemperatures(double *pData,double *meshX,double *meshY,int dim){
  char fileName[265];
  double *pDataAux;
  int h,k;
  int ncols=NX+1;
  int nrows=NY+1;
  double xllcorner=XMIN;
  double yllcorner=YMIN;
  double cellsize=DX;
  double val;
  double minT=1000;
  double maxT=0;
  pDataAux=pData;
  
  for (h=1;h<8760;h++){
    //lecture du raster
    sprintf(fileName,"/home/olivierb/Downloads/DATA_RASTER/TAIR/TAIR%i",h);
    double *pT=(double*)calloc(nrows*ncols,sizeof(double));
    double *pTaux=(double*)calloc(nrows*ncols,sizeof(double));
    FILE *infile=fopen(fileName,"rb");
    int nr=fread(pT, sizeof(double), ncols*nrows, infile);
    if (nr!=ncols*nrows){
      printf("Error fread return value= % instead of %i\n",nr,ncols*nrows);
      return;
    }
    if(DEBUGING){
      memcpy(pTaux,pT,sizeof(double)* ncols*nrows);
      for (k=0;k<ncols*nrows;k++){
        if (pTaux[k]<minT)
          minT=pTaux[k];
        if (pTaux[k]>maxT)
          maxT=pTaux[k];
      }
      printf ("minT=%lf maxT=%lf\n",minT,maxT);
      for (k=0;k<ncols*nrows;k++)
        pTaux[k]=pTaux[k]/maxT;
      
      

      
      visuTiff_Init(1,ncols,nrows);
      visuTiff_Register(pTaux,0);
      visuTiff_End();
      break;
    }
    
      
    fclose(infile);
    //creation du champ de temperature sur le maillage
    for(long int i=0; i<dim; i++)
    {
      double bufT;
      double Y=meshY[i];
      double X=meshX[i];
      int icol=(X-xllcorner)/((1+1e-6)*cellsize);
      int irow=(Y-yllcorner)/((1+1e-6)*cellsize);
      int index=icol+(irow)*ncols;
      if (index >= nrows*ncols || index <0){
        cout<<"bug "<<index<<" soulb be <"<<nrows*ncols;
        return ;
      }
      bufT=pT[index];
      *(pDataAux++)=bufT-273.15;
    }

    free(pT);
  }
    
}

void computeDegreJ(double T0,double Nd,double *pDataT,int dim, double * pDataDegJ){
  for (long int i=0;i<dim;i++)
    if ((*(pDataT++))>T0)
      *(pDataDegJ++)=1;
    else
      *(pDataDegJ++)=0;
  
  for (int h=1;h<365*24;h++){
    memcpy(pDataDegJ,pDataDegJ-dim,dim*sizeof(double));
    for (long int i=0;i<dim;i++){
      if ((*(pDataT++))>T0 && (*pDataDegJ) <Nd)
        (*pDataDegJ)+=1;
      pDataDegJ++;
    }
  }
  
}


void computeAverageT(double Period,double *pDataT,int dim,double *pDataAverage){
  double *pBuf=(double *)calloc(dim,sizeof(double));
  double *pAux=pBuf;
  double *pCur=pDataT;
  double *pAux2=pDataAverage;
  int h;
  for (h=0;h<Period;h++){
    pAux=pBuf;
    for (long int i=0;i<dim;i++){
      *(pAux++)+=(*pCur++);
      *(pDataAverage++)=0;
    }
  }
  double *pCurMinusT=pDataT;
  for(;h<24*365;h++){
    memcpy(pDataAverage,pBuf,dim*sizeof(double));
    pDataAverage+=dim;
    pAux=pBuf;
    for (long int i=0;i<dim;i++){
      *(pAux++)+=(*pCur++)-(*pCurMinusT++);
    }
  }
  free(pBuf);
  for(h=0;h<24*365*dim;h++){
    *pAux2=*pAux2/Period;
    pAux++;
  }
}
void computeAverageJanuaryT(double *pDataT,int dim, double *pBuf){
  
  double *pAux;
  for (int h=0;h<30*24;h++){
    pAux=pBuf;
    for (long int i=0;i<dim;i++){
      *(pAux++)+=(*pDataT++);
    }
  }
  pAux=pBuf;
  for (long int i=0;i<dim;i++){
    *pAux=*pAux/(24*30*1.0);
    pAux++;
  }
  

  
}

int main(){
  char fileName[256];
  int ncols;
  int nrows;
  double xllcorner;
  double yllcorner;
  double cellsize;
  double val;
  double minT=1000;
  double *pDatas,*pDatasDegJ,*pDatasAverage;
  double *pDataAux;
  long int dim;
  double *meshY,*meshX;
  double bufT;
  dim=readXY(&meshX,&meshY);
//temperature pDatas(num,h)=pDatas[num + h*dim];
  //ie, pDatas[num + h*dim]; est la température à l'heure h du sommet num
  pDatas=(double *)malloc(dim*24*365*sizeof(double));//temperature pDatas(num,h)=pDatas[num + h*dim];
  pDatasDegJ=(double *)malloc(dim*24*365*sizeof(double));//temperature pDatas(num,h)=pDatas[num + h*dim];
  
  readTemperatures(pDatas,meshX,meshY,dim);
  if (DEBUGING)
    return 0;
  pDataAux=pDatas;
  for (int h=0;h<24*365;h++){
    sprintf(fileName,"T_BY_HEURE/meshT%d",h);
    ofstream outfile (fileName,ios_base::binary);
    outfile.write ((char*) &dim, sizeof(long int));
    for(long int i=0; i<dim; i++){
      bufT=*(pDataAux++);
      outfile.write ((char*) &bufT, sizeof(double));
    }
    outfile.close();
  }
  return 0;
  computeDegreJ(17,240,pDatas,dim,pDatasDegJ);
  pDataAux=pDatasDegJ;
  for (int h=0;h<24*365;h++){
    sprintf(fileName,"DEGJ_BY_HEURE/meshDEGJ%d",h);
    ofstream outfile (fileName,ios_base::binary);
    outfile.write ((char*) &dim, sizeof(long int));
    for(long int i=0; i<dim; i++)
    {
      bufT=*(pDataAux++);
      outfile.write ((char*) &bufT, sizeof(double));
    }
    outfile.close();
  }
  pDatasAverage=(double *)malloc(dim*24*365*sizeof(double));
  computeAverageT(7*24,pDatas,dim,pDatasAverage);
  pDataAux=pDatasAverage;
  for (int h=0;h<24*365;h++){
    sprintf(fileName,"AVERAGE_BY_HEURE/meshAverage%d",h);
    ofstream outfile (fileName,ios_base::binary);
    outfile.write ((char*) &dim, sizeof(long int));
    for(long int i=0; i<dim; i++)
    {
      bufT=*(pDataAux++);
      outfile.write ((char*) &bufT, sizeof(double));
    }
    outfile.close();
  }

  double *pBuf=(double *)calloc(dim,sizeof(double));
  pDataAux=pBuf;
  computeAverageJanuaryT(pDatas,dim,pBuf);
  ofstream outfile ("januaryAverage",ios_base::binary);
  outfile.write ((char*) &dim, sizeof(long int));
  for(long int i=0; i<dim; i++)
    {
      bufT=*(pDataAux++);
      outfile.write ((char*) &bufT, sizeof(double));
    }
  outfile.close();
  free(pBuf);
  
  free(pDatasAverage);
  free(pDatas);
  free(pDatasDegJ);
  return 0;
}
