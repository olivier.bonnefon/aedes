#include "stdio.h"

int main(){
  for (int i=0;i<15;i++){
    printf("varf varfMatw0s2D%i(v,w)=int2d(Th%i)((alpha+alphaw0*ENDDIAPOS%i)*v*w);\n",i,i,i);
    printf("varf varfRhsw0s2D%i(v,w)= int2d(Th%i)((alpha*w0stm12d%i)*w);\n",i,i,i);
    printf("varf varfMatw1s2D%i(v,w)=int2d(Th%i)((alpha)*v*w);\n",i,i);
    printf("varf varfRhsw1s2D%i(v,w)= int2d(Th%i)((alpha*w1stm12d%i+alphaw1*BEGINDIAPOS%i*u2d%i)*w);\n\n",i,i,i,i,i);
  }

  printf("matrix =[\n\t[");
  for (int i=0;i<15;i++){
    for (int j=0;j<15;j++){
      if (i==j)
        printf("w0sM2D%i",i);
      else
        printf("0");
      if (j<14)
        printf(",");
    }
    printf("]");
    if (i<14)
      printf(",\n\t[");
  }
  printf("];\n\n");
  for (int i=0;i<15;i++)
    printf("matrix w0sM2D%i;\n",i);
  

  for (int i=0;i<15;i++)
    printf("matrix w1sM2D%i;\n",i);
  
  for (int i=0;i<15;i++)
    printf("rhsw0s2d%i,",i);

  printf("\n");
  
  for (int i=0;i<15;i++){
    printf("w0s2d%i[]=solw0(curDof:curDof+n2Ddofs%i-1);\n",i,i);
    printf("curDof=curDof+n2Ddofs%i;\n",i);
  }

for (int i=0;i<15;i++)
  printf("savevtk(OUTPUTDIR+\"D2/w0s%is\"+string(curStep)+\".vtk\",Th%i,w0stm12d%i,dataname=\"UU\");\n",i,i,i);


for (int i=0;i<15;i++)
   printf("n2Ddofs%i+",i);
for (int i=0;i<15;i++)
  printf("paraview w0s%is0.vtk\n",i);

  return 0;
}
