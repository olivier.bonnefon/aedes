This file is copied during the execution of the C prog gen2speciesNL.c.
It contains the command that must be done to allow the simulation.

This commands are necessary to run the simulation:
1) more userFDef0.txt
  Just to check the dynamic on each patch
2) chmod +x genSetFunc.sh
3) ./genSetFunc.sh
  To generate the dynamic on each patch


The simuated must be ready to run:
FreeFem++ cas1.edp

Finaly, visualized with 'python vtktopng.py'
