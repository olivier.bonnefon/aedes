import vtk
import os
from array import array
#baseName="/home/olivierb/VTK_PYTHON/diff/u"
#species=array('c','uv')

#numStep=1998
#for num in range(1,150):
#fenetre=vtk.vtkRenderWindow()
#fenetre.SetSize(500,500)

if (os.path.isfile('../buildActorWires.py')):
    print "wires actor has been generated"
else:
    print "WARNING: wires actor has not been generated"
    print "RUN: FF buildWire.edp"
    quit()

nbStep=50
step=20
nbOmega=2
WinSizeWidth=1400
WinSizeHeight=1000
firstStep=1
imgSuffix=".jpg"
#fenetre=vtk.vtkRenderWindow()
#fenetre.SetSize(WinSize,WinSize)
rgb=[0,0,0]
execfile("vtktopngGen.py")
os.chdir(OUTPUTDIR+"/D2");
rg=array('i',[1,2])
INTCURSPEC=0
INTCURSTEP=firstStep
INTCURYEAR=firstYear
if (os.path.isfile('curyear.txt')):
    fcurspec = open("curyear.txt", 'r')
    for line in fcurspec:
        for x in line.split():
            INTCURYEAR=int(x)
    fcurspec.close()

if (os.path.isfile('curstep.txt')):
    fcurstep = open("curstep.txt", 'r')
    for line in fcurstep:
        for x in line.split():
            INTCURSTEP=int(x)-1
    fcurstep.close();

print "CURSPEC "+str(INTCURSPEC)
print "CURSTEP "+str(INTCURSTEP)

curYear=INTCURYEAR;
numSpec=0
for spec in species:
    specname="umin"
    if (spec=='v'):
        specname="umax"
    if (numSpec < INTCURSPEC or doAvi[numSpec]==0):
        print "pass spec "+str(numSpec)
        numSpec=numSpec+1
        continue
    finput = open("input"+specname+".txt", 'w')
    baseName=OUTPUTDIR+"/D2/"+specname
    print "base name="+baseName
    for curYear in range(firstYear,firstYear+nbYear):
        for numStep in range(INTCURSTEP,nbStep):
            print "vtktojpg "+str(curYear)+" "+str(numStep)
            fenetre=vtk.vtkRenderWindow()
            fenetre.SetSize(WinSizeWidth,WinSizeHeight)
            ren=vtk.vtkRenderer()
            fenetre.AddRenderer(ren)
            execfile("/mnt/bigcalcul2/biom/olivierB/solvers/trunk/SandBox/FF/AEDESINTER_AUTOROUTES/buildActorWires.py")
            execfile("/mnt/bigcalcul2/biom/olivierB/solvers/trunk/SandBox/FF/AEDESINTER_AUTOROUTES/addActorWires.py")
            ren.SetBackground(1,1,1)
            for numOmega in range(0,nbOmega):
                fileName=baseName+str(numOmega)+"s"+str(curYear)+"_"+str(numStep*step)+".vtk"
                r=vtk.vtkGenericDataObjectReader()
                r.SetFileName(fileName)
                r.Update()
                o=r.GetOutput()
                o.GetCellData().SetActiveScalars("UU")
                aPolyVertexMapper = vtk.vtkDataSetMapper()
                aPolyVertexMapper.SetInput(o)
                aPolyVertexMapper.SetScalarRange(minV[numSpec],maxV[numSpec])
                aPolyVertexMapper.UseLookupTableScalarRange=1
                lut=aPolyVertexMapper.GetLookupTable()
                lut.SetHueRange(251.0/360.0,0)
                lut.SetSaturationRange(1,1)
                lut.SetValueRange(1,1)
                lut.Build()
                aPolyVertexActor = vtk.vtkActor()
                aPolyVertexActor.SetMapper(aPolyVertexMapper)
                ren.AddActor(aPolyVertexActor)
            scalarBar = vtk.vtkScalarBarActor()
            scalarBar.SetWidth(0.1)
            tprop = vtk.vtkTextProperty()
            tprop.SetColor(0.0,0.0,0.0)
            #tprop.SetFontSize(ax.getp('fontsize'))
            tprop.ShadowOff()
            scalarBar.SetLabelTextProperty(tprop)
            # scalarBar.SetLookupTable(lut)
            # Use this LUT if you want the highest value at the top.
            scalarBar.SetLookupTable(lut)
            #scalarBar.SetTitle('Elevation (m)')
            txt=vtk.vtkTextActor()
            txt.SetInput(str(curYear)+" "+str(numStep*step))
            txtprop=txt.GetTextProperty()
            txtprop.SetFontFamilyToArial()
            txtprop.SetFontSize(25)
            txtprop.BoldOn()
            txtprop.SetColor(0,0,0)
            txt.SetDisplayPosition(20,30)
            ren.AddActor(scalarBar);
            ren.AddActor(txt)
            #iren=vtk.vtkRenderWindowInteractor()
            #iren.SetRenderWindow(fenetre)
            #iren.Initialize()
            fenetre.Render()
            image = vtk.vtkWindowToImageFilter()
            image.SetInput(fenetre)
            image.Update()
            fenetre.Start()
            fenetre.Render()
            fenetre.Finalize()
            image.UpdateInformation()
            image.Update()
            #jpg = vtk.vtkTIFFWriter()
            if (imgSuffix==".jpg"):
                jpg = vtk.vtkJPEGWriter()
            else:
                jpg = vtk.vtkTIFFWriter()
            #fileStepName="step"+spec+str(numStep)+imgSuffix
            fileStepName=fileName.strip('.vtk')+imgSuffix
            jpg.SetFileName(fileStepName)
            jpg.SetInputConnection(image.GetOutputPort())
            jpg.Write()
            fcurstep = open("curstep.txt", 'w')
            fcurstep.write(str(numStep))
            fcurstep.close()
            finput.write(fileStepName+"\n");
    finput.close()
    print "mencoder \"mf://@input"+specname+".txt\" -mf fps=20 -o traj"+specname+".avi -ovc lavc -lavcopts vcodec=msmpeg4v2:vbitrate=1800"
numSpec+=1
INTCURSTEP=firstStep
fcurspec = open("curspec.txt", 'w')
fcurspec.write(str(numSpec))
fcurspec.close()


