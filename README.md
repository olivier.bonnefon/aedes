simulateur inter et intra annuel du moustique

liens :
https://journals.plos.org/plosone/article?id=10.1371/journal.pone.0151217
https://www.overleaf.com/13956933gvxcnyfhhwtc

AedesAlbopictus: contient des scripts de pré-calculs et des maillages

AEDESINTER_AUTOROUTES: version du simulateur sans pas de temps adaptif, n'a pas vocation a etre maintenu

AEDESINTER_AUTOROUTES_ADAPTIV: simulateur avec les donnees climatiques

INIT_W0: contient l'état initial des oeufs diaposants. les oeufs diaposant ayant survecus à l'hiver n n+1 sont sauver avec le suffix n.
