import vtk
import os
import os.path

from array import array
#baseName="/home/olivierb/VTK_PYTHON/diff/u"
#species=array('c','uv')

#numStep=1998
#for num in range(1,150):
#fenetre=vtk.vtkRenderWindow()
#fenetre.SetSize(500,500)

if (os.path.isfile('buildActorWires.py')):
    print "wires actor has been generated"
else:
    print "WARNING: wires actor has not been generated"
    print "RUN: FF buildWire.edp"
    quit()

nbStep=50
step=20
nbOmega=2
WinSizeWidth=1400
WinSizeHeight=1000
firstStep=1
imgSuffix=".jpg"
#fenetre=vtk.vtkRenderWindow()
#fenetre.SetSize(WinSize,WinSize)
rgb=[0,0,0]
execfile("vtktopngGen.py")
for curyear in range(2019,2020):
    if (curyear==2011):
        nbStep=122
    else:
        nbStep=364
    os.chdir(OUTPUTDIR+"/D2/"+str(curyear));
    rg=array('i',[1,2])
    INTCURSPEC=0
    INTCURSTEP=firstStep
    if (os.path.isfile('curspec.txt')):
        fcurspec = open("curspec.txt", 'r')
        for line in fcurspec:
            for x in line.split():
                INTCURSPEC=int(x)
        fcurspec.close()

    if (os.path.isfile('curstep.txt')):
        fcurstep = open("curstep.txt", 'r')
        for line in fcurstep:
            for x in line.split():
                INTCURSTEP=int(x)-1
        fcurstep.close();

    print "CURSPEC "+str(INTCURSPEC)
    print "CURSTEP "+str(INTCURSTEP)

    numSpec=0
    for spec in species:
        specname=spec
        if (spec=='r'):
            specname="RR"
        if (spec=='h'):
            specname="gamma1"
        if (spec=='v'):
            specname="w0"
        if (spec=='w'):
            specname="w1"
        if (spec=='g'):
            specname="gamma0"
        if (numSpec < INTCURSPEC or doAvi[numSpec]==0):
            print "pass spec "+str(numSpec)
            numSpec=numSpec+1
            continue
        finput = open("input"+specname+".txt", 'w')
        baseName=OUTPUTDIR+"/D2/"+str(curyear)+"/"+specname
        print "base name="+baseName
        for numStep in range(INTCURSTEP,nbStep):
            filenotexist=0
            print "vtktojpg "+str(numSpec)+" "+str(numStep)
            fenetre=vtk.vtkRenderWindow()
            fenetre.SetSize(WinSizeWidth,WinSizeHeight)
            ren=vtk.vtkRenderer()
            fenetre.AddRenderer(ren)
            execfile("../../buildActorWires.py")
            execfile("../../addActorWires.py")
        #execfile("buildActorWires2.py")
        #execfile("addActorWires2.py")
        #execfile("buildActorWires1.py")
        #execfile("addActorWires1.py")
            ren.SetBackground(1,1,1)
            #ren.ResetCamera(-2.1,6.1,44,49,0,0.1)
            for numOmega in range(0,nbOmega):
                fileName=baseName+str(numOmega)+"s"+str(numStep*step)+".vtk"
                if (not os.path.exists(fileName)):
                    filenotexist=1;
                    print "file not exist\n"
                    break;
                r=vtk.vtkGenericDataObjectReader()
                r.SetFileName(fileName)
                r.Update()
                o=r.GetOutput()
                o.GetCellData().SetActiveScalars("UU")
                aPolyVertexMapper = vtk.vtkDataSetMapper()
                aPolyVertexMapper.SetInput(o)
                aPolyVertexMapper.SetScalarRange(minV[numSpec],maxV[numSpec])
                aPolyVertexMapper.UseLookupTableScalarRange=1
                lut=aPolyVertexMapper.GetLookupTable()
                lut.SetHueRange(251.0/360.0,0)
                lut.SetSaturationRange(1,1)
                lut.SetValueRange(1,1)
                lut.Build()
                aPolyVertexActor = vtk.vtkActor()
                aPolyVertexActor.SetMapper(aPolyVertexMapper)
                ren.AddActor(aPolyVertexActor)
            if (filenotexist):
                break;
            scalarBar = vtk.vtkScalarBarActor()
            scalarBar.SetWidth(0.1)
            tprop = vtk.vtkTextProperty()
            tprop.SetColor(0.0,0.0,0.0)
            #tprop.SetFontSize(ax.getp('fontsize'))
            tprop.ShadowOff()
            scalarBar.SetLabelTextProperty(tprop)
            # scalarBar.SetLookupTable(lut)
            # Use this LUT if you want the highest value at the top.
            scalarBar.SetLookupTable(lut)
            #scalarBar.SetTitle('Elevation (m)')
            txt=vtk.vtkTextActor()
            txt.SetInput(specname+" "+str(numStep*step)+" "+str(curyear))
            txtprop=txt.GetTextProperty()
            txtprop.SetFontFamilyToArial()
            txtprop.SetFontSize(25)
            txtprop.BoldOn()
            txtprop.SetColor(0,0,0)
            txt.SetDisplayPosition(20,30)
            ren.AddActor(scalarBar);
            ren.AddActor(txt)
        #iren=vtk.vtkRenderWindowInteractor()
        #iren.SetRenderWindow(fenetre)
        #iren.Initialize()
            fenetre.Render()
            image = vtk.vtkWindowToImageFilter()
            image.SetInput(fenetre)
            image.Update()
            fenetre.Start()
            fenetre.Render()
            fenetre.Finalize()
            image.UpdateInformation()
            image.Update()
        #iren.Render()
        #iren.Render()
        #iren.Render()
            #jpg = vtk.vtkTIFFWriter()
            if (imgSuffix==".jpg"):
                jpg = vtk.vtkJPEGWriter()
            else:
                jpg = vtk.vtkTIFFWriter()
            fileStepName="step"+spec+str(numStep)+imgSuffix
            jpg.SetFileName(fileStepName)
            jpg.SetInputConnection(image.GetOutputPort())
            jpg.Write()
            fcurstep = open("curstep.txt", 'w')
            fcurstep.write(str(numStep))
            fcurstep.close()
            finput.write(fileStepName+"\n");

        finput.close()
        print "mencoder \"mf://@input"+specname+".txt\" -mf fps=20 -o traj"+specname+".avi -ovc lavc -lavcopts vcodec=msmpeg4v2:vbitrate=1800"
        numSpec+=1
        INTCURSTEP=firstStep
        fcurspec = open("curspec.txt", 'w')
        fcurspec.write(str(numSpec))
        fcurspec.close()


