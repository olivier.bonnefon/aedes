import vtk
import os
from array import array
#baseName="/home/olivierb/VTK_PYTHON/diff/u"
#species=array('c','uv')

#numStep=1998
#for num in range(1,150):
#fenetre=vtk.vtkRenderWindow()
#fenetre.SetSize(500,500)

WinSizeWidth=1400
WinSizeHeight=1000
firstStep=1
imgSuffix=".jpg"
#fenetre=vtk.vtkRenderWindow()
#fenetre.SetSize(WinSize,WinSize)
rgb=[0,0,0]


nbStep=120;
step=1;
firstStep=1;
nbOmega=15;
OUTPUTDIR="/media/biometrie/cabboud/SIMULATEUR/MODEL8_Travail/FF/D2/";
species=array('c','u');
umaxDensity=0.15
uminDensity=0
minV=array('d',[uminDensity ])
maxV=array('d',[umaxDensity ])


os.chdir(OUTPUTDIR);
rg=array('i',[1,2])
INTCURSPEC=0
INTCURSTEP=firstStep
fichier = open("test.txt", "r")

#for ligne in fichier:
#    l2 =ligne.rstrip('\n\r')
#    print l2

finput = open("input.txt", 'w')
numSpec=0
spec='u'
baseName=OUTPUTDIR
numStep=0
#for numStep in range(INTCURSTEP,nbStep):
for ligne in fichier:
    l2 =ligne.rstrip('\n\r')
    #print "vtktojpg "+str(numSpec)+" "+str(numStep)
    fenetre=vtk.vtkRenderWindow()
    fenetre.SetSize(WinSizeWidth,WinSizeHeight)
    ren=vtk.vtkRenderer()
    fenetre.AddRenderer(ren)
    ren.SetBackground(1,1,1)
    numOmega=0
    fileName=baseName+l2
    titre=l2.strip('.vtk')
    #fileName=baseName+spec+str(numOmega)+"s"+str(numStep*step)+".vtk"
    #fileName=baseName+l2
    print fileName
    r=vtk.vtkGenericDataObjectReader()
    r.SetFileName(fileName)
    r.Update()
    o=r.GetOutput()
    o.GetCellData().SetActiveScalars("UU")
    aPolyVertexMapper = vtk.vtkDataSetMapper()
    aPolyVertexMapper.SetInput(o)
    aPolyVertexMapper.SetScalarRange(minV[numSpec],maxV[numSpec])
    aPolyVertexMapper.UseLookupTableScalarRange=1
    lut=aPolyVertexMapper.GetLookupTable()
    lut.SetHueRange(251.0/360.0,0)
    lut.SetSaturationRange(1,1)
    lut.SetValueRange(1,1)
    lut.Build()
    scalarBar = vtk.vtkScalarBarActor()
    scalarBar.SetWidth(0.1)
    tprop = vtk.vtkTextProperty()
    tprop.SetColor(0.0,0.0,0.0)
    #tprop.SetFontSize(ax.getp('fontsize'))
    tprop.ShadowOff()
    scalarBar.SetLabelTextProperty(tprop)
    # scalarBar.SetLookupTable(lut)
    # Use this LUT if you want the highest value at the top.
    scalarBar.SetLookupTable(lut)
    #scalarBar.SetTitle('Elevation (m)')
    txt=vtk.vtkTextActor()
    txt.SetInput(titre[3:6])
    txtprop=txt.GetTextProperty()
    txtprop.SetFontFamilyToArial()
    txtprop.SetFontSize(18)
    txtprop.SetColor(0,0,0)
    txt.SetDisplayPosition(20,30)
    aPolyVertexActor = vtk.vtkActor()
    aPolyVertexActor.SetMapper(aPolyVertexMapper)
    ren.AddActor(aPolyVertexActor)
    ren.AddActor(scalarBar);
    ren.AddActor(txt)
    fenetre.Render()
image = vtk.vtkWindowToImageFilter()
    image.SetInput(fenetre)
    image.Update()
    fenetre.Start()
    fenetre.Render()
    fenetre.Finalize()
    image.UpdateInformation()
    image.Update()
    #jpg = vtk.vtkTIFFWriter()
    if (imgSuffix==".jpg"):
        jpg = vtk.vtkJPEGWriter()
    else:
        jpg = vtk.vtkTIFFWriter()
    fileStepName=l2.strip('.vtk')+imgSuffix
    finput.write(fileStepName+"\n");
    jpg.SetFileName(fileStepName)
    jpg.SetInputConnection(image.GetOutputPort())
    jpg.Write()
    numStep=numStep+1
        

finput.close()

#os.system("mencoder \"mf://@input.txt\" -mf fps=20 -o test.avi -ovc lavc -lavcopts vcodec=msmpeg4v2:vbitrate=1800")
print "mencoder \"mf://@input.txt\" -mf fps=20 -o test.avi -ovc lavc -lavcopts vcodec=msmpeg4v2:vbitrate=1800"
#os.system("vlc test.avi")
   

