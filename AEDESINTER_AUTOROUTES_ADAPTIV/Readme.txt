Programme pricipale FF: cas1.edp avec commentaires 

usage :
  simulation avec assimilations: Freefem++ cas1.edp 
  -----------------------------
  simule sur la periode 2008--2019 en utilisant la procedure d'assimilation
   T0 de 2008 est 1-122/365 ie 1 septembre
   T0 de 2009-2019 est 59/365 ie 1 mars

   construction de la donnee initiale
     en 2008 la donne init est nulle, puis le 1 septembre assimilation des donnees 2008 avec thetaAssim=1
     en y=2009 .. 2019 la donnee est contenue dans la variable w0s2d0.
     
     
   Sauvegarde des oeux pour curyear ds {2008,..,2019}:
     w1s2d(i=0)_(n=numrun)_(y=curyear) : etat des oeux a la fin de l'annee curyear
     w0s2d(i=0)_(n=numrun)_(y=curyear) : etat des oeux ayant passe l'hiver de [dec curyear, fev curyear+1] (a utiliser pour la donnee initial de l'annee curyear+1


   simulation sans assimilation: Freefem++ FIRSTYEAR NBPREDYEAR 
   -----------------------------
   simule sur la periode [1 fev FIRSTYEAR +(NBPREDYEAR-1), dec FIRSTYEAR +NBPREDYEAR]
   
      il s'agit ici de mesurer la qualite predictive du modele avec assimilation prealable simile (voir ci-dessus).
      FIRSTYEAR ds {2009,2018}
      NBPREDYEAR tel que FIRSTYEAR+NBPREDYEAR <= 2019  

      ATTENTION: suppose que Freefem++ FIRSTYEAR NBPREDYEAR-1, avec NBPREDYEAR-1 >0 a ete joue 
      

   construction de la donnee initiale:
     * NBPREDYEAR=1: on va chercher les oeux ayant passe l'hiver [dec FIRSTYEAR-1,fev FIRSTYEAR] lors de la simu avec assimilation, cela revient a charger w0s2d(i=0)_(n=numrun)_(FIRSTYEAR-1)
     * NBPREDYEAR>1: on va chercher l'etat final de l'appel Freefem++ FIRSTYEAR NBPREDYEAR-1, cela revient a charger w0s2d_(NBPREDYEAR-1)_FIRSTYEAR


   sauvegarde tout les jours simulés:
     u2d0_NBPREDYEAR_FIRSTYEAR_rep_(n=numRun) sauvegarde des 365 jours simulé, utile pour le calcul du BS

   sauvegarde en fin de simu 
     w1s2d0_(n=numrun)_(NBPREDYEAR)_(FIRSTYEAR) : oeux diaposant en dec FIRSTYEAR +NBPREDYEAR
     w0s2d0_(n=numrun)_(NBPREDYEAR)_(FIRSTYEAR) : oeux en fev FIRSTYEAR +NBPREDYEAR+1 ayant passe l'hiver, sera utilisé comme donnée initiale pour Freefem++ FIRSTYEAR NBPREDYEAR+1.

   
