#include <fstream>
#include  <iostream>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
using namespace std;
#define BASEREP "../SAVE_LOW/"

long int GetSizeVec(char* name){
  std::ifstream infile (name,ios_base::binary);
  long int dim;
  infile.read((char *) &dim, sizeof(long int));
  infile.close();
  return dim;
}
void LoadVec(char* name,double *pD)   
{
  std::ifstream infile (name,ios_base::binary);
  long int dim;
  infile.read((char *) &dim, sizeof(long int));
  double dtemp;
  for(long int i=0; i<dim; i++)
  {
   infile.read((char *) &dtemp, sizeof(double));
    *(pD++)=dtemp ;
  }
  infile.close();
}
void SaveVec(double * pD, long int N, char* name)   
{
  std::ofstream outfile (name,ios_base::binary);
  
  
  outfile.write ((char*) &N, sizeof(long int));//write the dimension of the vector
  double ftemp ;
  for(long int i=0; i<N; i++) {
    
    ftemp = pD[i] ;
    outfile.write ((char*) &ftemp, sizeof(double));
  }
  outfile.close();
}

int main(){
  char filename[256];
  int N=100;
  double * pBufTraj[15];
  double * pMaxTraj[15];
  double * pMinTraj[15];
  long int size[15];
  for (int j=0;j<15;j++){
    sprintf(filename,"%su2d%i__rep_0",BASEREP,j);
    size[j]=GetSizeVec(filename);
    pBufTraj[j]=(double *)malloc(size[j]*sizeof(double));
    pMaxTraj[j]=(double *)malloc(size[j]*sizeof(double));
    pMinTraj[j]=(double *)malloc(size[j]*sizeof(double));
    LoadVec(filename,pMaxTraj[j]);
    memcpy(pMinTraj[j],pMaxTraj[j],size[j]*sizeof(double));
  }
  
  for(int numRun=1;numRun<N;numRun++){
    for (int j=0;j<15;j++){
      sprintf(filename,"%su2d%i__rep_%i",BASEREP,j,numRun);
      LoadVec(filename,pBufTraj[j]);
      for (int k=0;k<size[j];k++){
        if (pBufTraj[j][k]>pMaxTraj[j][k])
          pMaxTraj[j][k]=pBufTraj[j][k];
        if (pBufTraj[j][k]<pMinTraj[j][k])
          pMinTraj[j][k]=pBufTraj[j][k];
      }
    }
  }

  for (int j=0;j<15;j++){
    sprintf(filename,"%su2d%i__max",BASEREP,j);
    SaveVec(pMaxTraj[j],size[j],filename);
    sprintf(filename,"%su2d%i__min",BASEREP,j);
    SaveVec(pMinTraj[j],size[j],filename);
  }
  for (int j=0;j<15;j++){
    free(pBufTraj[j]);
    free(pMaxTraj[j]);
    free(pMinTraj[j]);
    
  }
  
  return 0;
}
